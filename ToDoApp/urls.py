"""ToDoApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include

from task.views import ShowTask, ShowTaskDetail, MakeDone
from user.views import CreateUser, LandingPageView, LogInView

urlpatterns = [
    path("", LandingPageView.as_view()),
    path('admin/', admin.site.urls),
    path("task/", include('task.urls')),
    path("tasklist", ShowTask.as_view(), name="show_task"),
    path("taskdetail/<int:pk>/", ShowTaskDetail.as_view(), name="task_detail"),
    path("maketaskdone/<int:pk>", MakeDone.as_view(), name="task_done"),
    path("createuser/", CreateUser.as_view(), name="create_user"),
    path("login/", LogInView.as_view(), name="login")
]
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


