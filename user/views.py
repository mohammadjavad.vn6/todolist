from django.contrib.auth import login
from django.shortcuts import render

# Create your views here.
from django.urls import reverse_lazy
from django.views.generic import FormView, TemplateView

from task.views import ShowTask
from user.forms import CreateUserForm, LoginForm


class CreateUser(FormView):
    form_class = CreateUserForm
    template_name = "user/register.html"
    success_url = reverse_lazy('show_task')

    def form_valid(self, form):
        form.save()
        login(self.request, form.cleaned_data["user_login"])
        return super().form_valid(form)


class LandingPageView(TemplateView):
    template_name = "landingpage.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data()
        if self.request.user.is_authenticated:
            context["authenticated"] = True
        if self.request.user.is_anonymous:
            context["anonymous"] = True
        return context


class LogInView(FormView):
    form_class = LoginForm
    template_name = "user/login.html"
    success_url = "/tasklist"

    def form_valid(self, form):
        login(self.request, form.cleaned_data["user"])
        return super().form_valid(form)