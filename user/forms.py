from django import forms
from django.contrib.auth import get_user_model, authenticate
from django.utils.translation import ugettext_lazy as _

from user.models import Profile

User = get_user_model()


class CreateUserForm(forms.Form):
    username = forms.CharField(max_length=20)
    email = forms.EmailField(widget=forms.EmailInput)
    password = forms.CharField(widget=forms.PasswordInput)
    password_confirmation = forms.CharField(widget=forms.PasswordInput)
    image = forms.ImageField()

    def clean(self):
        if not "username" in self.cleaned_data:
            raise forms.ValidationError(_("Username Field is Required"))
        if User.objects.filter(username=self.cleaned_data["username"]).exists():
            raise forms.ValidationError(_("User with this username already exists"))
        if not "password" in self.cleaned_data:
            raise forms.ValidationError(_("Password Field is Required"))
        if not "password_confirmation" in self.cleaned_data:
            raise forms.ValidationError(_("Password confirmation Field is Required"))
        if self.cleaned_data['password'] != self.cleaned_data['password_confirmation']:
            raise forms.ValidationError(_("The two password fields didn't match."))

    def save(self):
        user = User.objects.create_user(
            username=self.cleaned_data["username"],
            email=self.cleaned_data["email"],
            password=self.cleaned_data["password"]
        )
        profile = Profile.objects.create(
            avatar=self.cleaned_data["image"], user=user
        )
        user_login = authenticate(**self.cleaned_data)
        self.cleaned_data["user_login"] = user_login
        return user, profile, user_login


class LoginForm(forms.Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        if not "username" in self.cleaned_data:
            raise forms.ValidationError(_("Username Field is Required"))
        if not "password" in self.cleaned_data:
            raise forms.ValidationError(_("Password Field is Required"))
        user = User.objects.filter(username=self.cleaned_data["username"]).first()
        if user is None:
            raise forms.ValidationError(_("User with provided username does not exists"))
        if not user.check_password(self.cleaned_data["password"]):
            raise forms.ValidationError(_("Wrong password"))
        user = authenticate(**self.cleaned_data)
        if user is None:
            raise forms.ValidationError(_("Unable login with provided credentials"))
        self.cleaned_data["user"] = user

        return self.cleaned_data