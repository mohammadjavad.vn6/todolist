from django.contrib.auth import get_user_model

from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from lib.model import BaseModel

User = get_user_model()


class Profile(BaseModel):
    avatar = models.ImageField(
        verbose_name=_("avatar"), upload_to="user/profile"
    )
    user = models.OneToOneField(
        User, related_name="user", verbose_name=_("user"),
        on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = "profile"
        verbose_name_plural = "profiles"

    def __str__(self):
        return self.user.username

