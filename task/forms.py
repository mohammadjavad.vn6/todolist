from django.forms import ModelForm, TimeInput, TextInput
#from bootstrap3_datetime.widgets import DateTimePicker
from task.models import Task


class TaskForm(ModelForm):
    class Meta:
         model = Task
         fields = (
             "title", "description", "attachment", "image", "time",
             "date_time", "priority"
         )
         widgets = {
             'time': TextInput(
                 attrs={'type': 'time'}
             ),
             "date_time": TextInput(
                 attrs={'type': 'date'}
             )
         }

    def save(self, commit=False, user=None):
        task = Task.objects.create(
            title=self.cleaned_data["title"],
            description=self.cleaned_data["description"],
            attachment=self.cleaned_data["attachment"],
            image=self.cleaned_data["image"],
            time=self.cleaned_data["time"],
            date_time=self.cleaned_data["date_time"],
            is_done=False,
            priority=self.cleaned_data["priority"],
            user=user
        )
        return task

