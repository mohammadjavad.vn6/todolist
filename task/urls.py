from django.urls import path

from task.views import CreateTaskForm, ShowTask

urlpatterns = [
    path("create/", CreateTaskForm.as_view(), name="create_task"),
    path("create/done/", ShowTask.as_view())
]