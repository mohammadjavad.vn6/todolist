from django.db import models

# Create your models here.
from django.utils.translation import ugettext_lazy as _

from lib.model import BaseModel
from task.validators import validate_file_extension
from user.models import Profile


class Task(BaseModel):
    NECESSARY = 1
    IMPORTANT = 2
    USUAL = 3
    INDIFFERENT = 4
    priority_choice = (
        (NECESSARY, "necessary"),
        (IMPORTANT, "important"),
        (USUAL, "usual"),
        (INDIFFERENT, "indifferent"),
    )
    title = models.CharField(max_length=90, verbose_name=_("title"))
    description = models.TextField(verbose_name=_("description"), blank=True)
    attachment = models.FileField(
        verbose_name=_("attachment"), validators=[validate_file_extension],
        upload_to="task/CreateTask/attachments/", blank=True
    )
    image = models.ImageField(
        verbose_name=_("image"), upload_to="task/CreateTask/image/", blank=True
    )
    time = models.TimeField(verbose_name=_("time"), blank=True, null=True)
    date_time = models.DateField(verbose_name=_("date time"), blank=True, null=True)
    is_done = models.BooleanField(verbose_name=_("is done"), default=False)
    priority = models.IntegerField(
        verbose_name=_("priority"), choices=priority_choice
    )
    user = models.ForeignKey(
        Profile, verbose_name=_("user"), on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _("task")
        verbose_name_plural = _("tasks")

    def __str__(self):
        return self.title