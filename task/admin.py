from django.contrib import admin

# Register your models here.
from task.models import Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "time", "date_time", "priority", "is_done")
    list_filter = ("is_done", "priority")
    search_fields = ("fields", "date_time")
    list_display_links = ("title", )
    actions = ("make_done", )

    def make_done(self, request, queryset):
        queryset.update(is_done=True)
        return queryset


admin.site.register(Task, TaskAdmin)