from django.shortcuts import render, redirect

# Create your views here.
from django.views import View
from django.views.generic import FormView, ListView, DetailView, RedirectView

from task.forms import TaskForm
from task.models import Task


class CreateTaskForm(FormView):
    form_class = TaskForm
    template_name = "task/createtask.html"
    success_url = "done/"

    def form_valid(self, form):
        form.save(commit=True, user=self.request.user.user)
        return super().form_valid(form)


class ShowTask(ListView):
    model = Task
    template_name = "task/tasklist.html"

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(user=self.request.user.user, is_done=False).all()
        return queryset.all()


class ShowTaskDetail(DetailView):
    model = Task
    template_name = "task/taskdetail.html"
    context_object_name = "detail"

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.filter(pk=self.kwargs["pk"])
        return queryset


class MakeDone(View):
    # def get_object(self):
    #     task = Task.objects.get(pk=self.kwargs["pk"])
    #     return task

    def post(self, request, *args, **kwargs):
        #target_task = self.get_object()
        # if target_task.pk == self.kwargs["pk"]:
        Task.objects.filter(pk=self.kwargs["pk"]).update(is_done=True)
        return redirect("/tasklist")
